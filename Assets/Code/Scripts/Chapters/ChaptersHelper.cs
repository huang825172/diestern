using System.Collections.Generic;
using UnityEngine;

namespace Code.Scripts.Chapters
{
    public class ChaptersHelper : MonoBehaviour
    {
        public List<GameObject> chapters;

        public Dictionary<string, GameObject> ChaptersDic
        {
            get
            {
                var dic = new Dictionary<string, GameObject>();
                foreach (var chapter in chapters)
                {
                    dic[chapter.name] = chapter;
                }

                return dic;
            }
        }
    }
}