using UnityEngine;

namespace Code.Scripts
{
    public class CharacterController : MonoBehaviour
    {
        private const float Epsilon = 0.0001f;
        public static CharacterController ActiveController;

        private static readonly int Running = Animator.StringToHash("Running");
        private static readonly int Speed = Animator.StringToHash("Speed");

        public float speed;
        private readonly Quaternion _backwardRotation = Quaternion.AngleAxis(180, Vector3.up);

        private readonly Quaternion _forwardRotation = Quaternion.AngleAxis(0, Vector3.up);
        private Animator _animator;
        private Rigidbody2D _rigidbody;

        private void Start()
        {
            _animator = GetComponent<Animator>();
            _rigidbody = GetComponent<Rigidbody2D>();
        }

        private void Update()
        {
            if (speed > Epsilon)
            {
                transform.rotation = _forwardRotation;
                _animator.SetBool(Running, true);
                _animator.SetFloat(Speed, 1);
            }
            else if (speed < -Epsilon)
            {
                transform.rotation = _backwardRotation;
                _animator.SetBool(Running, true);
                _animator.SetFloat(Speed, 1);
            }
            else
            {
                _animator.SetBool(Running, false);
                _animator.SetFloat(Speed, 1);
                _rigidbody.velocity = Vector2.right * 0;
            }

            _rigidbody.velocity = Vector2.right * speed;
        }

        private void OnEnable()
        {
            ActiveController = this;
        }

        private void OnDisable()
        {
            if (ActiveController == this) ActiveController = null;
        }
    }
}