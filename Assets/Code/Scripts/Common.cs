using System;

namespace Code.Scripts
{
    public class Common
    {
#if UNITY_IOS || UNITY_ANDROID || UNITY_WP_8_1
        public static readonly bool IsMobile = true;
#else
        public static readonly bool IsMobile = false;
#endif
        public static void Swap<T>(ref T lhs, ref T rhs)
        {
            var temp = lhs;
            lhs = rhs;
            rhs = temp;
        }
    }

    public static class Extensions
    {
        public static T Next<T>(this T src) where T : struct
        {
            if (!typeof(T).IsEnum) throw new ArgumentException($"Argument {typeof(T).FullName} is not an Enum");

            var arr = (T[]) Enum.GetValues(src.GetType());
            var j = Array.IndexOf(arr, src) + 1;
            return arr.Length == j ? arr[0] : arr[j];
        }
    }
}