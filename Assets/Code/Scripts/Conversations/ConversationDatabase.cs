using System.Collections.Generic;
using UnityEngine;

namespace Code.Scripts.Conversations
{
    [CreateAssetMenu(fileName = "NewConversationDatabase", menuName = "Conversations/ConversationDatabase", order = 0)]
    public class ConversationDatabase : ScriptableObject
    {
        public List<Chat> chats;

        public Dictionary<string, Chat> ChatDictionary
        {
            get
            {
                var dic = new Dictionary<string, Chat>();
                foreach (var chat in chats)
                {
                    dic[chat.title] = chat;
                }

                return dic;
            }
        }
    }
}