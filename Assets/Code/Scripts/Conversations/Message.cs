using UnityEngine;

namespace Code.Scripts.Conversations
{
    [CreateAssetMenu(fileName = "NewMessage", menuName = "Conversations/Message", order = 0)]
    public class Message : ScriptableObject
    {
        public Person from;
        public string message;
    }
}