using System.Collections.Generic;
using System.Linq;
using Code.Scripts.Conversations;
using Code.Scripts.UI.Conversations;
using UnityEngine;

namespace Code.Scripts.EventBook
{
    public class EventBookManager : Singleton<EventBookManager>
    {
        public const string PlayerPrefsKey = "GameProgress";

        private List<string> _chats;

        private ConversationDatabase _database;

        private void Awake()
        {
            _database =
                Resources.Load<ConversationDatabase>("Conversations/ConversationDatabase");
            if (PlayerPrefs.HasKey(PlayerPrefsKey))
                _chats = JsonUtility.FromJson<List<string>>(PlayerPrefs.GetString(PlayerPrefsKey));

            if (_chats == null) _chats = new List<string>();
        }

        public void Save()
        {
            PlayerPrefs.SetString(PlayerPrefsKey, JsonUtility.ToJson(_chats));
        }

        public void Clear()
        {
            PlayerPrefs.DeleteKey(PlayerPrefsKey);
            _chats.Clear();
            Save();
        }

        public void ShowChat(Chat chat, bool addToEventBook = true)
        {
            if (addToEventBook)
            {
                var eventBook = UI.EventBook.EventBook.ActiveEventBook;
                var shower = Shower.ActiveShower;

                if (eventBook == null || shower == null) return;

                InitEventBookIfNeeded(eventBook);
                _chats.Add(chat.title);
                eventBook.Add(chat);
                shower.Show(chat);
            }
            else
            {
                var shower = Shower.ActiveShower;
                if (shower == null) return;

                shower.Show(chat);
            }
        }

        private void InitEventBookIfNeeded(UI.EventBook.EventBook eventBook)
        {
            if (eventBook.hasInit) return;
            eventBook.Clear();
            foreach (var chat in _chats.Select(title => GetChatByTitle(title))) eventBook.Add(chat);

            eventBook.hasInit = true;
        }

        public Chat GetChatByTitle(string title)
        {
            return _database.ChatDictionary[title];
        }
    }
}