using Code.Scripts.Conversations;
using UnityEngine;

namespace Code.Scripts.Experiments
{
    public class AddMessagesToEventBook : MonoBehaviour
    {
        public bool oneByOne = true;
        public Chat chat;

        private int _currentIndex;

        public void Add()
        {
            var eventBook = UI.EventBook.EventBook.ActiveEventBook;
            if (!eventBook) return;
            if (oneByOne)
            {
                var message = chat.messages[_currentIndex];
                _currentIndex++;
                if (_currentIndex >= chat.messages.Count)
                    _currentIndex = 0;
                eventBook.Add(message);
            }
            else
            {
                eventBook.Add(chat);
            }
        }

        public void ToggleMode()
        {
            oneByOne = !oneByOne;
        }

        public void ClearEventBook()
        {
            var eventBook = UI.EventBook.EventBook.ActiveEventBook;
            if (!eventBook) return;
            eventBook.Clear();
        }
    }
}