using System.Collections;
using UnityEngine;

namespace Code.Scripts.Experiments
{
    public class ScreenLogger : MonoBehaviour
    {
        private const int MaxChars = 10000;
        private string _log;
        private readonly Queue _myLogQueue = new Queue();

        private void Start()
        {
            Debug.Log("Screen logger started");
        }

        private void Update()
        {
            while (_myLogQueue.Count > 0)
                _log = _myLogQueue.Dequeue() + _log;
            if (_log.Length > MaxChars)
                _log = _log.Substring(0, MaxChars);
        }

        private void OnEnable()
        {
            Application.logMessageReceived += HandleLog;
        }

        private void OnDisable()
        {
            Application.logMessageReceived -= HandleLog;
        }

        private void OnGUI()
        {
            GUILayout.Label(_log);
        }

        private void HandleLog(string logString, string stackTrace, LogType type)
        {
            _myLogQueue.Enqueue("\n [" + type + "] : " + logString);
            if (type == LogType.Exception)
                _myLogQueue.Enqueue("\n" + stackTrace);
        }
    }
}