using System;
using System.Linq;
using Code.Scripts.Chapters;
using UnityEngine;

namespace Code.Scripts.Experiments
{
    public class SwitchToHome : MonoBehaviour
    {
        public void Switch()
        {
            var obj = Resources
                .FindObjectsOfTypeAll<GameObject>()
                .FirstOrDefault(g => g.CompareTag("Chapter"));
            Debug.Log(obj);
            obj.SetActive(true);
            // ArchiveManager.Instance.NewGame();
            // GameStatusManager.Instance.status = GameStatusManager.Status.Gaming;
        }

        private void Update()
        {
            // ArchiveManager.Instance.NewGame();
            // GameProgressManager.Instance.CurrentChapterGameObject().SetActive(true);
        }
    }
}