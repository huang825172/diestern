using Code.Scripts.InventorySystem;
using Code.Scripts.UI.Interactable.InspectItemCanvas;
using UnityEngine;

namespace Code.Scripts.Interactable
{
    [RequireComponent(typeof(Item))]
    public class CollectableInspector : MonoBehaviour, IInspector
    {
        public void Inspect()
        {
            var inspectItemCanvas = InspectItemCanvas.ActiveInspectItemCanvas;
            if (inspectItemCanvas == null) return;

            inspectItemCanvas.Show(GetComponent<Item>());
            Collected = true;
        }

        public bool Collected { get; private set; }
    }
}