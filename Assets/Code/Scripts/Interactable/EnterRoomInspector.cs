using Code.Scripts.Chapters;
using UnityEngine;

namespace Code.Scripts.Interactable
{
    public class EnterRoomInspector : MonoBehaviour, IInspector
    {
        public Chapter chapter;
        public Room toEnter;
        
        public void Inspect()
        {
            chapter.EnterRoom(toEnter.title);
        }

        public bool Collected { get; } = false;
    }
}