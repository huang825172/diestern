using Code.Scripts.InventorySystem;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Code.Scripts.Interactable
{
    public class Interactable : MonoBehaviour, IPointerDownHandler
    {
        public float thresholdDistance = 10.0f;
        public Material highlightMaterial;

        private Material _defaultMaterial;

        private bool _highlighted;

        private Inventory _inventory;
        private IInspector Inspector => GetComponent<IInspector>();
        private SpriteRenderer SpriteRenderer => GetComponent<SpriteRenderer>();

        private static GameObject Character => CharacterController.ActiveController.gameObject;

        private bool ClosedToPlayer =>
            Vector3.Distance(transform.position, Character.transform.position) <= thresholdDistance;

        // private static Inventory Inventory => Singleton<Inventory>.Instance;
        private Item Item => GetComponent<Item>();

        private void Start()
        {
            _inventory = Inventory.Instance;
            _defaultMaterial = SpriteRenderer.material;
        }

        private void Update()
        {
            if (Inspector != null && Inspector.Collected)
            {
                if (Item && _inventory) _inventory.AddItem(Item.title);
                gameObject.transform.SetParent(null);
                Destroy(gameObject);
                return;
            }

            if (thresholdDistance < 0)
            {
                HighLight();
                return;
            }

            if (ClosedToPlayer)
            {
                if (_highlighted) return;
                HighLight();
            }
            else
            {
                Unhighlight();
            }
        }

        private void OnMouseDown()
        {
            Inspector.Inspect();
            Debug.Log("Inspect!");
        }

        private void OnMouseEnter()
        {
            HighLight();
        }

        private void OnMouseExit()
        {
            Unhighlight();
        }

        private void HighLight()
        {
            _highlighted = true;
            SpriteRenderer.material = highlightMaterial;
        }

        private void Unhighlight()
        {
            _highlighted = false;
            SpriteRenderer.material = _defaultMaterial;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            Inspector.Inspect();
            Debug.Log("Inspect!");
        }
    }
}