using Code.Scripts.Conversations;
using Code.Scripts.EventBook;
using UnityEngine;

namespace Code.Scripts.Interactable
{
    public class ShowChatInspector : MonoBehaviour, IInspector
    {
        public Chat chat;

        private bool _alreadyTriggered;

        public void Inspect()
        {
            EventBookManager.Instance.ShowChat(chat, !_alreadyTriggered);
            _alreadyTriggered = true;
        }

        public bool Collected { get; } = false;
    }
}