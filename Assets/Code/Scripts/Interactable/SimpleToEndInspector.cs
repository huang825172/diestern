using Code.Scripts.Chapters;
using UnityEngine;

namespace Code.Scripts.Interactable
{
    public class SimpleToEndInspector : MonoBehaviour, IInspector
    {
        public Chapter chapter;
        
        public void Inspect()
        {
            chapter.NextChapter();
        }

        public bool Collected { get; } = false;
    }
}