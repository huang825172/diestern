using UnityEngine;

namespace Code.Scripts
{
    public class PauseManager : Singleton<PauseManager>
    {
        public bool IsPaused { get; private set; }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape) &&
                GameStatusManager.Instance.status != GameStatusManager.Status.InMainMenu) Toggle();
        }

        public void Toggle()
        {
            IsPaused = !IsPaused;
            PauseIfNeeded();
        }

        private void PauseIfNeeded()
        {
            Time.timeScale = IsPaused ? 0 : 1;
        }

        public void Pause()
        {
            IsPaused = true;
            PauseIfNeeded();
        }

        public void Unpause()
        {
            IsPaused = false;
            PauseIfNeeded();
        }
    }
}