using UnityEngine;

namespace Code.Scripts
{
    public class SettingsManager : Singleton<SettingsManager>
    {
        private const string VolumeKey = "volume";

        private const string PlayerPrefsAudioVolumeKey = "volume";

        public float AudioVolume
        {
            get
            {
                if (PlayerPrefs.HasKey(PlayerPrefsAudioVolumeKey))
                {
                    var volume = PlayerPrefs.GetFloat(PlayerPrefsAudioVolumeKey);
                    AudioListener.volume = volume;
                    return volume;
                }

                PlayerPrefs.SetFloat(PlayerPrefsAudioVolumeKey, 0.5f);
                AudioListener.volume = 0.5f;
                return 0.5f;
            }
            set
            {
                var finalValue = 0.0f;
                if (value < 0) finalValue = 0.0f;
                else if (value > 1) finalValue = 1f;
                else finalValue = value;
                AudioListener.volume = finalValue;
                PlayerPrefs.SetFloat(PlayerPrefsAudioVolumeKey, finalValue);
            }
        }

        private void Start()
        {
            Debug.Log($"Initial Volume: {AudioVolume}");
            Application.targetFrameRate = 300;
        }

        protected override void OnDestroyOrQuit()
        {
            PlayerPrefs.Save();
        }
    }
}