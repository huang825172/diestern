using UnityEngine;

namespace Code.Scripts.UI.Controller
{
    public class Controller : MonoBehaviour
    {
        public static Controller ActiveController;
        public Holdable left;
        public Holdable right;
        private CharacterController _character;

        private void Update()
        {
            if (_character == null) _character = CharacterController.ActiveController;
            if (PauseManager.Instance.IsPaused || _character == null) return;

            if (left.pressing > 0)
                _character.speed = -(left.pressing / 100f);
            else if (right.pressing > 0)
                _character.speed = right.pressing / 100f;
            else
                _character.speed = 0;
        }

        private void OnEnable()
        {
            ActiveController = this;
        }

        private void OnDisable()
        {
            if (ActiveController == this) ActiveController = null;
        }

        public void Hide()
        {
            left.gameObject.SetActive(false);
            right.gameObject.SetActive(false);
        }

        public void Show()
        {
            left.gameObject.SetActive(true);
            right.gameObject.SetActive(true);
        }
    }
}