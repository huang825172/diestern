using UnityEngine;
using UnityEngine.EventSystems;

namespace Code.Scripts.UI.Conversations
{
    public class TapArea : MonoBehaviour, IPointerDownHandler
    {
        public MessageCanvas canvas;
        
        public void OnPointerDown(PointerEventData eventData)
        {
            Debug.Log("Tap!");
            canvas.shouldShowNext = true;
        }
    }
}