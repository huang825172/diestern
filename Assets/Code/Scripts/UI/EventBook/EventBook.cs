using System.Collections.Generic;
using Code.Scripts.Conversations;
using UnityEngine;

namespace Code.Scripts.UI.EventBook
{
    public class EventBook : MonoBehaviour
    {
        public static EventBook ActiveEventBook;

        public bool hasInit;

        public GameObject content;

        public GameObject messageItemPrefab;

        private void OnEnable()
        {
            ActiveEventBook = this;
        }

        private void OnDisable()
        {
            hasInit = false;
            if (ActiveEventBook == this) ActiveEventBook = null;
        }

        public void Add(Message message)
        {
            var messageItemObject =
                Instantiate(messageItemPrefab, content.transform, true);
            var messageItem = messageItemObject.GetComponent<MessageItem>();
            messageItem.message = message;
            messageItemObject.transform.SetAsFirstSibling();
        }

        public void Add(Chat chat)
        {
            foreach (var message in chat.messages) Add(message);
        }

        public void Add(List<Message> messages)
        {
            foreach (var message in messages) Add(message);
        }
        
        public void Clear()
        {
            var childCount = content.transform.childCount;
            for (var i = 0; i < childCount; i++)
            {
                var child = content.transform.GetChild(i);
                // child.SetParent(null);
                Destroy(child.gameObject);
            }

            hasInit = false;
        }
    }
}