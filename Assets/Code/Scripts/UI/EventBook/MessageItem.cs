using Code.Scripts.Conversations;
using UnityEngine;
using UnityEngine.UI;

namespace Code.Scripts.UI.EventBook
{
    public class MessageItem : MonoBehaviour
    {
        public Message defaultMessage;
        public Message message;
        
        public Image avatarImage;
        public Text nameText;
        public Text messageText;

        private void Update()
        {
            if (message)
                ShowMessage(message);
            else
                ShowMessage(defaultMessage);
        }

        private void ShowMessage(Message msg)
        {
            avatarImage.sprite = msg.from.avatar;
            nameText.text = msg.from.name;
            messageText.text = msg.message;
        }
    }
}