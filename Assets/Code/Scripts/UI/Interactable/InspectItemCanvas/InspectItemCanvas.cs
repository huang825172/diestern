using Code.Scripts.InventorySystem;
using UnityEngine;
using UnityEngine.UI;

namespace Code.Scripts.UI.Interactable.InspectItemCanvas
{
    public class InspectItemCanvas : MonoBehaviour
    {
        public static InspectItemCanvas ActiveInspectItemCanvas;

        public GameObject displayArea;

        public Image iconImage;
        public Text titleText;
        public Text descriptionText;

        public bool showing;

        private void Update()
        {
            if (showing)
            {
                PauseManager.Instance.Pause();
                displayArea.SetActive(true);
            }
            else
            {
                PauseManager.Instance.Unpause();
                displayArea.SetActive(false);
            }
        }

        private void OnEnable()
        {
            ActiveInspectItemCanvas = this;
        }

        private void OnDisable()
        {
            if (ActiveInspectItemCanvas == this)
                ActiveInspectItemCanvas = null;
        }

        public void Show(Item item)
        {
            iconImage.sprite = item.icon;
            iconImage.preserveAspect = true;
            titleText.text = item.title;
            descriptionText.text = item.description;
            displayArea.SetActive(true);
            showing = true;
        }
    }
}