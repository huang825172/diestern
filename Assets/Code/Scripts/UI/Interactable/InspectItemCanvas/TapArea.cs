using UnityEngine;
using UnityEngine.EventSystems;

namespace Code.Scripts.UI.Interactable.InspectItemCanvas
{
    public class TapArea : MonoBehaviour, IPointerDownHandler
    {
        public InspectItemCanvas canvas;

        public void OnPointerDown(PointerEventData eventData)
        {
            canvas.showing = false;
        }
    }
}