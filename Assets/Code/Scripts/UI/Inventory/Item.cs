using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Code.Scripts.UI.Inventory
{
    [ExecuteAlways]
    public class Item : MonoBehaviour,
        IBeginDragHandler, IEndDragHandler, IDragHandler,
        IDropHandler,
        IPointerDownHandler, IPointerUpHandler
    {
        public Sprite defaultIcon;
        public bool maskOn;
        public bool selected;

        private Vector3 _beginPosition;
        private CanvasGroup _canvasGroup;

        private GameObject _count;
        private int _countNum;
        private Text _countText;
        private bool _dragEnd;
        private bool _dropHandled;
        private Image _icon;

        private Sprite _iconSprite;

        private Vector2 _lastMousePosition;
        private GameObject _mask;

        private int _pointerDown;
        private RectTransform _rectTransform;
        private GameObject _selectedMask;
        private GameObject _shortcut;

        private int _shortcutId = -1;
        private Text _shortcutText;
        private bool _shouldPlaceBack;
        public ItemData ItemData = new ItemData();

        private void Start()
        {
            Init();
        }

        private void Update()
        {
            if (_pointerDown > 0 && _pointerDown < 60) _pointerDown++;
            if (_pointerDown > 50) ItemData.ShouldShowTooltip = true;

            if (!ItemData.IsEmpty())
            {
                _iconSprite = ItemData.Icon;
                _countNum = ItemData.Count;
            }

            _icon.sprite = _iconSprite == null ? defaultIcon : _iconSprite;
            _countText.text = _countNum.ToString();
            _mask.SetActive(maskOn);
            _selectedMask.SetActive(selected);

            switch (_countNum)
            {
                case 0:
                    _mask.SetActive(false);
                    _count.SetActive(false);
                    _selectedMask.SetActive(false);
                    _icon.sprite = defaultIcon;
                    selected = false;
                    return;
                case 1:
                    _count.SetActive(false);
                    return;
            }

            _count.SetActive(true);
        }

        private void FixedUpdate()
        {
            if (_shouldPlaceBack) _rectTransform.position = _beginPosition;
            if (_dragEnd && !_dropHandled)
            {
                Debug.Log("should show delete confirm");
                ItemData.ShouldDisplayDeleteConfirm = true;
            }

            _dropHandled = _dragEnd = false;
            _shouldPlaceBack = false;
        }

        private void OnEnable()
        {
            Init();
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            _beginPosition = _lastMousePosition =
                _rectTransform.position;
            _canvasGroup.blocksRaycasts = false;
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (ItemData.Count == 0) return;
            var currentMousePosition = eventData.position;
            var diff = currentMousePosition - _lastMousePosition;
            var position = _rectTransform.position;
            var newPosition = position + new Vector3(diff.x, diff.y, transform.position.z);
            var oldPos = position;
            position = newPosition;
            _rectTransform.position = position;
            if (!IsRectTransformInsideScreen(_rectTransform))
                _rectTransform.position = oldPos;

            _lastMousePosition = currentMousePosition;
        }

        public void OnDrop(PointerEventData eventData)
        {
            var draggingObject = eventData.pointerDrag;

            handleDrop:
            if (draggingObject != null && draggingObject != gameObject)
            {
                var item = draggingObject.GetComponent<Item>();
                if (item == null) return;
                Common.Swap(ref item.ItemData, ref ItemData);
                Common.Swap(ref item.maskOn, ref maskOn);
                Common.Swap(ref item.ItemData, ref ItemData);
                // Common.Swap(ref item.selected, ref selected);
                item._dropHandled = true;
                item.Update();
                Update();
            }
            else
            {
                draggingObject = eventData.pointerEnter;
                goto handleDrop;
            }
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            _shouldPlaceBack = true;
            _canvasGroup.blocksRaycasts = true;
            _dragEnd = true;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            _pointerDown = 1;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            _pointerDown = 0;
            ItemData.ShouldShowTooltip = true;
        }

        private void Init()
        {
            _count = transform.Find("Count").gameObject;
            _countText = _count.transform.Find("Number").GetComponent<Text>();
            _icon = transform.Find("Icon").GetComponent<Image>();
            _shortcut = transform.Find("Shortcut").gameObject;
            _shortcutText = _shortcut.GetComponent<Text>();
            _mask = transform.Find("Mask").gameObject;
            _rectTransform = GetComponent<RectTransform>();
            _selectedMask = transform.Find("SelectedMask").gameObject;
            _canvasGroup = GetComponent<CanvasGroup>();

            if (Regex.IsMatch(name, @"^Item\d$"))
            {
                _shortcut.SetActive(true);
                _shortcutText.text = name[4].ToString();
                _shortcutId = name[4] - '0';
            }
            else
            {
                _shortcut.SetActive(false);
            }

            _selectedMask.SetActive(selected);

            Update();
        }

        private static bool IsRectTransformInsideScreen(RectTransform rectTransform)
        {
            var isInside = false;
            var corners = new Vector3[4];
            rectTransform.GetWorldCorners(corners);
            var rect = new Rect(0, 0, Screen.width, Screen.height);
            var visibleCorners = corners.Count(corner => rect.Contains(corner));
            if (visibleCorners == 4) isInside = true;
            return isInside;
        }

        public void Clear()
        {
            ItemData.Clear();
            Update();
        }

        public bool IsEmpty()
        {
            return ItemData.IsEmpty();
        }
    }
}