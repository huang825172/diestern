using UnityEngine;

namespace Code.Scripts.UI.Inventory
{
    public class ItemData
    {
        public InventorySystem.Item Item = null;
        public int Count = 0;
        public bool ShouldDisplayDeleteConfirm = false;
        public bool ShouldClear = false;
        public bool ShouldShowTooltip = false;
        
        public string Title => Item.title;
        public string Description => Item.description;

        public Sprite Icon => Item.icon;

        public void Clear()
        {
            Item = null;
            ShouldClear = ShouldShowTooltip = ShouldDisplayDeleteConfirm = false;
            Count = 0;
        }

        public bool IsEmpty()
        {
            if (Item == null || Count == 0)
            {
                Clear();
                return true;
            }

            return false;
        }
    }
}