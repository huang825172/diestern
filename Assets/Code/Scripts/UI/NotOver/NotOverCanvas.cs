using UnityEngine;

namespace Code.Scripts.UI.NotOver
{
    public class NotOverCanvas : MonoBehaviour
    {
        public static NotOverCanvas ActiveNotOverCanvas;
        public bool end;
        public GameObject displayArea;

        public bool showing;

        private void Start()
        {
            displayArea.SetActive(false);
        }

        private void Update()
        {
            if (end)
            {
                PauseManager.Instance.Unpause();
                displayArea.SetActive(false);
                return;
            }

            if (showing)
            {
                PauseManager.Instance.Pause();
                displayArea.SetActive(true);
            }
        }

        private void OnEnable()
        {
            ActiveNotOverCanvas = this;
        }

        private void OnDisable()
        {
            if (ActiveNotOverCanvas == this) ActiveNotOverCanvas = null;
        }

        public void Show()
        {
            showing = true;
        }
    }
}