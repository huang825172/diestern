using UnityEngine;
using UnityEngine.EventSystems;

namespace Code.Scripts.UI.NotOver
{
    public class TapToEnd : MonoBehaviour, IPointerDownHandler
    {
        public NotOverCanvas master;

        public void OnPointerDown(PointerEventData eventData)
        {
            master.end = true;
        }
    }
}