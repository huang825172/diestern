using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Video;

namespace Code.Scripts.UI.NotOver
{
    public class TapToShowNext : MonoBehaviour, IPointerDownHandler
    {
        public NotOverCanvas master;

        public GameObject notOverText;
        public GameObject lateNightText;
        public GameObject videoPlayer;

        private GameObject _activeObject;

        private State _current = State.NotOverText;

        private bool _stopped;

        private void Start()
        {
            _activeObject = notOverText;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            AdvanceState();
        }

        private void SetActiveObject(GameObject obj)
        {
            if (_activeObject) _activeObject.SetActive(false);
            if (!obj) return;
            _activeObject = obj;
            obj.SetActive(true);
        }

        private void AdvanceState()
        {
            if (_stopped) return;
            _current = _current.Next();
            switch (_current)
            {
                case State.LateNightText:
                    SetActiveObject(lateNightText);
                    break;
                case State.Video:
                    SetActiveObject(videoPlayer);
                    videoPlayer.GetComponent<VideoPlayer>().loopPointReached += CheckOver;
                    break;
                case State.End:
                    SetActiveObject(null);
                    master.end = true;
                    _stopped = true;
                    break;
                case State.NotOverText:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void CheckOver(VideoPlayer vp)
        {
            if (_current == State.Video)
                AdvanceState();
        }

        private enum State
        {
            NotOverText,
            LateNightText,
            Video,
            End
        }
    }
}