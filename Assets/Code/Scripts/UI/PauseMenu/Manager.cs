using UnityEngine;

namespace Code.Scripts.UI.PauseMenu
{
    public class Manager : MonoBehaviour
    {
        public void ExitToMainMenu()
        {
            GameStatusManager.Instance.status = GameStatusManager.Status.InMainMenu;
            ArchiveManager.Instance.Save();
            PauseManager.Instance.Unpause();
            WindowManager.ShouldCloseAll = true;
        }
    }
}