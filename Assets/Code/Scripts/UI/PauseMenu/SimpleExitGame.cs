using UnityEngine;

namespace Code.Scripts.UI.PauseMenu
{
    public class SimpleExitGame : MonoBehaviour
    {
        public void ExitGame()
        {
            Application.Quit();
        }
    }
}