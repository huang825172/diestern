using System;
using UnityEngine;
using UnityEngine.UI;

namespace Code.Scripts.UI.SettingMenu
{
    public class VolumeSliderHelper : MonoBehaviour
    {
        private Slider Slider => GetComponent<Slider>();

        private void Start()
        {
            Slider.minValue = 0.0f;
            Slider.maxValue = 1.0f;
            Slider.value = SettingsManager.Instance.AudioVolume;
        }

        private void Update()
        {
            SettingsManager.Instance.AudioVolume = Slider.value;
        }

        private void OnEnable()
        {
            Slider.value = SettingsManager.Instance.AudioVolume ;
        }
    }
}