using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Code.Scripts.UI
{
    public class WindowManager : MonoBehaviour, IPointerDownHandler
    {
        public GameObject window;
        public bool responseWhilePausing;

        public UnityEvent onWindowClosed;
        public UnityEvent onWindowOpen;
        public UnityEvent onWindowDisable;
        public UnityEvent onWindowEnable;

        public bool IsClosed { get; private set; } = true;

        private void Update()
        {
            if (!responseWhilePausing && PauseManager.Instance.IsPaused)
                return;
            if (ShouldCloseAll)
            {
                CloseWindow();
                ShouldCloseAll = false;
                CurrentActive = null;
                return;
            }

            if (Input.GetKeyDown(KeyCode.Escape) && CurrentActive != null)
            {
                CurrentActive.CloseWindow();
                if (PauseManager.Instance.IsPaused)
                    PauseManager.Instance.Unpause();
            }
        }

        private void OnEnable()
        {
            window.SetActive(!IsClosed);
            onWindowEnable.Invoke();
            if (!IsClosed)
                onWindowOpen.Invoke();
        }

        private void OnDisable()
        {
            window.SetActive(false);
            onWindowDisable.Invoke();
        }

        public void CloseWindow()
        {
            if (!responseWhilePausing && PauseManager.Instance.IsPaused)
                return;
            IsClosed = true;
            window.SetActive(false);
            onWindowClosed.Invoke();
        }

        public void OpenWindow()
        {
            if (!responseWhilePausing && PauseManager.Instance.IsPaused)
                return;
            IsClosed = false;
            window.SetActive(true);
            onWindowOpen.Invoke();
        }

        public void Toggle()
        {
            if (!responseWhilePausing && PauseManager.Instance.IsPaused)
                return;
            window.SetActive(!window.activeSelf);
            if (window.activeSelf)
                onWindowOpen.Invoke();
            else
                onWindowClosed.Invoke();
        }

        public static bool ShouldCloseAll = false;

        private static WindowManager CurrentActive;
        
        public void OnPointerDown(PointerEventData eventData)
        {
            CurrentActive = this;
        }
    }
}